/// <reference types="Cypress" />
import Home from'../../support/pages/Home'
import Shop from'../../support/pages/Shop'

describe('Test Suite', () => {

    const homePage = new Home()
    const shopPage = new Shop()

    let dataObj = {}

    before(() => cy.fixture('example').then(data => dataObj = data))
    
    it('FramweworkThing', () => {
        
        
        cy.visit(Cypress.env('url')+'/angularpractice')
        const username = homePage.getEditUserBox()
        username.type(dataObj.name)
        homePage.getSelect().select(dataObj.gender)
        homePage.getDataBindingBox().should('have.value', dataObj.name)
        username.should('have.attr', 'minlength', '2')
        homePage.getEntrepreneurRadioBtn().should('be.disabled')
        homePage.getShopNav().click();
        
        dataObj.productName.forEach(element => shopPage.selectProduct(element));

        shopPage.getCheckoutBtn().click()

        shopPage.sumProdcutPrice()
        
        shopPage.verifyTotalSum()  

        shopPage.getTotalCheckoutBtn().click()

        shopPage.getDeliveryCountry().type(dataObj.country)

        shopPage.getSuggestions().click()

        shopPage.acceptTermsAndCondition().click({force:true})

        shopPage.purchaseBtn().click()
        
        shopPage.verifyPurchasedProduct()

        

        //Cypress.config('defaultCommandTimeout', 8000)


   
    })
})