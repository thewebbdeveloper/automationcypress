/// <reference types="Cypress" />

describe('Test Suite', () => {
    
    it('My second testcase', ()=>{
        //visit url->
        cy.visit('https://fmpnextgen.scania.com/#/sign-in')
        cy.get('.sign-in-form').as('signUpForm')
        cy.get('@signUpForm').each(($el, index, $list)=>{
            const texts = $el.find('.control-label').text()
            if(texts.includes('Användarnamn')){
                cy.get('#username').type('kssuks');
            }
            if(texts.includes('Lösenord')){
                cy.get(':nth-child(2) > .form-control').type('ScaniaFleetApp123')
            }
        })
       cy.contains('Logga in').click()
       cy.wait(5000)
       cy.get('.ng-scope > .text-warning').should('have.text','News')
       
    })

   
})