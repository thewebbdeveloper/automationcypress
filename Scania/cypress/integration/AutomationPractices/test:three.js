/// <reference types="Cypress" />



describe('Test Suite', () => {
    
    it('My third testcase', ()=>{
        //visit url->
        cy.visit(Cypress.env('url')+'/AutomationPractice')
        
        //Checkboxes
        cy.get('#checkBoxOption1').check().should('be.checked').and('have.value','option1')
        cy.get('#checkBoxOption1').uncheck().should('not.be.checked')
        cy.get('input[type="checkbox"]').check(['option2', 'option3']).should('be.checked')
       
        //Static Dropbox
        cy.get('select').select('Option2').should('have.value', 'option2')

        //Dynamic Dropbox
        cy.get('#autocomplete').type('swe')
        cy.get('.ui-menu-item div').as('ulmenu')

        cy.get('@ulmenu').each(($el, index, $list) => {
           
            if($el.text() ==='Sweden'){
                cy.get('@ulmenu').click();
            }
        })

        cy.get('#autocomplete').should('have.value', 'Sweden')

        //Visability
        cy.get('#displayed-text').should('be.visible')
        cy.get('#hide-textbox').click()
        cy.get('#displayed-text').should('not.be.visible')
        cy.get('#show-textbox').click()
        cy.get('#displayed-text').should('be.visible')


        //Radio button
        cy.get('[value="radio2"]').check().should('be.checked')

        //Alert automeion be defaul it accepts but run callbacks if custom

        cy.get('#alertbtn').click()
        cy.get('[value="Confirm"]').click()



    })

   
})