/// <reference types="Cypress" />



describe('Test Suite', () => {
    
    it('My third testcase', ()=>{
        //visit url->
        cy.visit(Cypress.env('url')+'/AutomationPractice')

        //Alert automeion be defaul it accepts but run callbacks if custom

        cy.get('#alertbtn').click()
        cy.get('[value="Confirm"]').click()
        cy.on("window:alert", (res)=>{
            expect(res).to.equal('Hello , share this practice page and share your knowledge')
        })

        cy.on("window:confirm", (res)=> {  
            
            expect(res).to.equal('Hello , Are you sure you want to confirm?')
        
        })


        //Open tab remove it and open it on same browser 
        cy.get('#opentab').invoke('removeAttr', 'target').click()
        cy.url().should('include', 'rahulshettyacademy')
        cy.go('back')



    })

   
})