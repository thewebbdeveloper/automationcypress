/// <reference types="Cypress" />



describe('Test Suite', () => {


    
    it('My third testcase', ()=>{
        //visit url->
        cy.visit(Cypress.env('url')+'/AutomationPractice')

       //table rendering next value
        cy.get('tr td:nth-child(2)').as('courses')

        cy.get('@courses').each(($el, index, $list) => {
            const courses = $el.text()
            if(courses.includes("Python")){
                cy.get('@courses').eq(index).next().then((tblprice)=>{
                    const price = tblprice.text();
                    expect(price).to.equal('25')
                    
        
                })
            }
        })

        //handlin mouse over

        /*
        With jquery to get the popup
        cy.get('div.mouse-hover-content').invoke('show')
        */
         //without jquery
        cy.contains('Top').click({force: true})
        cy.url().should('include', 'top')

        //getting the urel froma window
        cy.get("#opentab").then((el)=>{
            const url = el.prop('href')
            cy.log(url)
        })





    })

   
})