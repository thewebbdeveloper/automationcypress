class Home {
   
    getEditUserBox = () => cy.get("input[name='name']:nth-child(1)")

    getDataBindingBox = () => cy.get("input[name='name']:nth-child(2)")
    
    getSelect = () => cy.get('select')

    getEntrepreneurRadioBtn = () => cy.get("#inlineRadio3")

    getShopNav = () => cy.contains("Shop")


}

export default Home