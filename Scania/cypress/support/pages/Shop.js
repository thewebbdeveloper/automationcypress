class Shop{
    
    constructor(sum) {this.sum = 0;}

    selectProduct = (productName) => cy.selectProduct(productName)

    getCheckoutBtn = () => cy.get('a.btn.btn-primary')

    getTotalCheckoutBtn = () => cy.get(':nth-child(4) > :nth-child(5) > .btn')

    getDeliveryCountry = () => cy.get('#country')

    getSuggestions = () => cy.get('.suggestions ul li a') 

    acceptTermsAndCondition = () => cy.get("#checkbox2")

    purchaseBtn = () => cy.get("input[type='submit']")

    verifyPurchasedProduct = () => cy.get('.alert').then((text)=> expect(text.text().includes('Success')).to.be.true)

    sumProdcutPrice = () => cy.get('tr td:nth-child(4) strong').each(($el, index, $list) => this.sum += Number($el.text().split(" ")[1].trim()))

    verifyTotalSum = () => cy.get('tr td:nth-child(5) h3 strong').then((text) => expect(this.sum).to.equal(Number(text.text().split(" ")[1].trim())))

    
}

export default Shop;